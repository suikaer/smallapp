<?php
error_reporting(1);
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/CreatorJwt.php';
require APPPATH . '/libraries/RestController.php';
require APPPATH . '/libraries/Format.php';
use Restserver\Libraries\RestController;

class Login extends RestController {
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */



    public function __construct() {
        parent::__construct();         
        $this->load->database();
        $this->load->model('login_model');
        $this->load->model('package_model');
        $this->jwtTokenObj = new CreatorJwt();
		
    }


    public function index() {       
        echo "WELCOME HERE!";
    }
	
   

/*************************************************
      DATE :August 2020
      FUNCTION : for login functionality
      Author: Suika
     ***************************************************/
    public function login_post() {      
        $formdata = json_decode(file_get_contents('php://input'), true);       
        $username = $formdata['userName'];
        $password = $formdata['password'];
        $postArray = array("userName" => $username, "password" => $password);
        validatePostData($postArray);              
        $data = $this->login_model->checklogin($username, $password);
        if ($data->userId == 0) {           
            log_error_msg('error', 'Oops! User Name OR Password is Wrong..!');
        } else {
            $sess = array('id' => $data->userId, 'firstname' => $data->firstName, 'lastname' => $data->lastName, 'is_logged_in' => 1, 'token' => uniqid(),'user' => true);
            $this->session->set_userdata('labsession', $sess);
            $user_data = $this->session->userdata('labsession');
            $tokenData['uniqueId'] = '11';
            $tokenData['role'] = $username;
            $tokenData['timeStamp'] = Date('Y-m-d h:i:s');
            $jwtToken = $this->jwtTokenObj->GenerateToken($tokenData);
            $result = array('message' => 'login successful', 'status' => 1, 'user' => $user_data, 'token' => $jwtToken);
            $this->response($result);
        }
   
    }


}
?>
