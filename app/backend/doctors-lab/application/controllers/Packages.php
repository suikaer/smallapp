<?php
error_reporting(1);
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/CreatorJwt.php';
require APPPATH . '/libraries/RestController.php';
require APPPATH . '/libraries/Format.php';
use Restserver\Libraries\RestController;

class Packages extends RestController {
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */



    public function __construct() {
        parent::__construct();       
        $this->load->database();
        $this->load->model('package_model');
        $this->jwtTokenObj = new CreatorJwt();
		
    }


    public function index() {       
        echo "WELCOME HERE!";
    }
	
	
 /*************************************************
      DATE :August 2020
      FUNCTION : for lab test listing
      Author: Suika
     ***************************************************/
    public function getAllPackages_get(){
    $receivedToken = $this->input->get_request_header('Authorization');
        if (!$receivedToken) {
            $result = array("Error" => "Access Denied",'status'=>403);
            $this->response($result);
        } else {
            $jwtData = $this->jwtTokenObj->DecodeToken($receivedToken);
            if (!empty($jwtData)) {
                $data = $this->package_model->getAllPackages();
                $result = array('message' => 'List Of Packages', 'status' => 1, 'list' => $data);
                $this->response($result);
            }
        }

    }



    /******************************************************************************************
     Purpose: To get the current user cart count info
    Parameters: UserId
    ********************************************************************************************/
    function cartcount_post() {     
        $receivedToken = $this->input->get_request_header('Authorization');
        if (!$receivedToken) {
            $result = array("Error" => "Access Denied",'status'=>403);
            $this->response($result);
        } else {
            $jwtData = $this->jwtTokenObj->DecodeToken($receivedToken);
            if (!empty($jwtData)) {
                $formdata = json_decode(file_get_contents('php://input'), true);
                $userId = $formdata['userId'];
                $postArray = array("userId" => $userId);
                validatePostData($postArray); 
                $data = $this->package_model->cartcount($userId);
                $result = array('message' => 'Cart count', 'status' => 1, 'count' => $data);
                $this->response($result);
            }
        }
    }


    /******************************************************************************************
    Purpose: Get all list of Lab-tests stored in databse of particular user
    Parameters: userId
    ********************************************************************************************/
    function getCartList_post() {
      
        $receivedToken = $this->input->get_request_header('Authorization');
        if (!$receivedToken) {
            $result = array("Error" => "Access Denied",'status'=>403);
            $this->response($result);
        } else {
            $jwtData = $this->jwtTokenObj->DecodeToken($receivedToken);
            if (!empty($jwtData)) {
                $formdata = json_decode(file_get_contents('php://input'), true);
                $userId = $formdata['userId'];
                $postArray = array("userId" => $userId);
                validatePostData($postArray); 
                $data = $this->package_model->getCartList($userId);
                $result = array('message' => 'Cart List', 'status' => 1, 'list' => $data);
                $this->response($result);
            }
        }
    }



 /******************************************************************************************
    Purpose: Delete pacakge from the cart of current user
    Parameters: packgeId
    ********************************************************************************************/

    function delUsercart_post() {
      
        $receivedToken = $this->input->get_request_header('Authorization');
        if (!$receivedToken) {
             $result = array("Error" => "Access Denied",'status'=>403);
             $this->response($result);
        } else {
            $jwtData = $this->jwtTokenObj->DecodeToken($receivedToken);
            if (!empty($jwtData)) {
                $formdata = json_decode(file_get_contents('php://input'), true);
                $packgeId = $formdata['packgeId'];
                $userId = $formdata['userId'];
                $postArray = array("userId" => $userId,"packgeId" => $packgeId);
                validatePostData($postArray); 
                $data = $this->package_model->delUsercart($packgeId);
                $cartcount = $this->package_model->cartcount($userId);
                if ($data == 1) {
                    $result = array('message' => 'Product Deleted From cart Successfully', 'status' => 1, 'cartcount' => $cartcount);
                    $this->response($result);
                } else {
                    $result = array('message' => 'Problem Occurs', 'status' => 0, 'cartcount' => $cartcount);
                    $this->response($result);
                }
            }
        }
    }





   /******************************************************************************************
    Purpose: To insert Test info in database added by a user
    Parameters: Product Id, User Id
    ********************************************************************************************/
    function addcart_post() {    
        $receivedToken = $this->input->get_request_header('Authorization');
        if (!$receivedToken) {
            $result = array("Error" => "Access Denied",'status'=>403);
            $this->response($result);
        } else {
            $jwtData = $this->jwtTokenObj->DecodeToken($receivedToken);
            if (!empty($jwtData)) {
                $formdata = json_decode(file_get_contents('php://input'), true);
                $packageId = $formdata['packageId'];
                $userId = $formdata['userId'];
                $postArray = array("userId" => $userId,"packageId" => $packageId);
                validatePostData($postArray); 
                $data = $this->package_model->addPackageInCart($packageId, $userId);
                $cartcount = $this->package_model->cartcount($userId);                
                if ($data == 1) {
                    $result = array('message' => 'Package Added Successfully', 'status' => 1,'cartcount'=>$cartcount);
                     $this->response($result);
                } else {
                    $result = array('message' => 'There is some Problem while Adding', 'status' => 0);
                    $this->response($result);
                }
            }
        }
    }



     /******************************************************************************************
    Purpose: Delete pacakge from the cart of current user
    Parameters: packgeId
    ********************************************************************************************/

    function emptyUserCart_post() {
      
        $receivedToken = $this->input->get_request_header('Authorization');
        if (!$receivedToken) {
             $result = array("Error" => "Access Denied",'status'=>403);
             $this->response($result);
        } else {
            $jwtData = $this->jwtTokenObj->DecodeToken($receivedToken);
            if (!empty($jwtData)) {
                $formdata = json_decode(file_get_contents('php://input'), true);                
                $userId = $formdata['userId'];
                $postArray = array("userId" => $userId);
                validatePostData($postArray); 
                $data = $this->package_model->emptyUserCart($userId);
                $cartcount = $this->package_model->cartcount($userId);
                if ($data == 1) {
                    $result = array('message' => 'Packages Deleted From cart Successfully', 'status' => 1, 'cartcount' => $cartcount);
                    $this->response($result);
                } else {
                    $result = array('message' => 'Problem Occurs', 'status' => 0, 'cartcount' => $cartcount);
                    $this->response($result);
                }
            }
        }
    }


}
?>
