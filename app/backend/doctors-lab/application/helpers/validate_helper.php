<?php

function validatePostData($post = array()) {
  $ci = & get_instance();
  $returnData = array('error' => "", 'success' => TRUE,);
  foreach ($post as $pkey => $pdata) {
    if ($pdata == "" || $pdata == "null" || $pdata == "NULL") {
      log_error_msg('error', "Parameter Missing : (" . $pkey . ") or has null value");
      break;
    }
    elseif($pdata == "undefined"){
      log_error_msg('error', "Parameter: (" . $pkey . ") is undefined");
      break;
    }
  }
}



function log_error_msg($status, $msg, $responseMsg = '') {
  $ci = & get_instance();
  $rmsg = ($responseMsg != '') ? $responseMsg : $msg;
  $data = array('status' => 0, 'message' => $rmsg, 'data' => "");
  $ci->response($data, 200);
}

?>