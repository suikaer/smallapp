<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Package_model extends CI_Model {
    public function __construct() {
        // Call the Model constructor
        parent::__construct();
    }


    /******************************************************************************************
    Purpose: Fetch all the list of packages
    ********************************************************************************************/
    function getAllPackages() {
        $this->db->select('*');
        $this->db->from('tbl_packages');
        $query = $this->db->get();
        return $query->result();
    }

  /******************************************************************************************
    Purpose: Fetch packages list by Id
    ********************************************************************************************/
    function getPackagesById($packageId) {
        $this->db->select('*');
        $this->db->from('tbl_packages');
          $this->db->where('id',$packageId);
        $query = $this->db->get();
        return $query->result();
    }



    

   /******************************************************************************************   
    Purpose: Cart count info of current user
    Parameters: userId
    ********************************************************************************************/
     function cartcount($userId) {      
        $this->db->select('IFNULL(sum(testCount),0) as totalCount');
        $this->db->from('tbl_cart');
        $this->db->where('userId', $userId);
        $this->db->where('status', 1);
        $query = $this->db->get();
        $num = $query->result();   
       // echo "<pre>"; print_r($num[0]->totalCount);die;     
        return $num[0]->totalCount;
    }


   /******************************************************************************************
    Purpose: Get all list of Lab-tests stored in databse of particular user
    Parameters: userId
    ********************************************************************************************/
     function getCartList($userId) {
        $this->db->select('p.id,p.itemName,p.minPrice,c.testCount');
        $this->db->from('tbl_cart c');
        $this->db->join('tbl_packages p', 'p.id=c.packageId', 'LEFT');
        $this->db->where('c.userId', $userId);
        $this->db->where('c.status', 1);
        $query = $this->db->get();
        return $query->result();
    }

  
  /******************************************************************************************
    Purpose: Delete pacakge from the cart of current user
    Parameters: packgeId
    ********************************************************************************************/

    function delUsercart($packageId) {
        $this->db->set('status', 0);
        $this->db->where('packageId', $packageId);
        $query = $this->db->update('tbl_cart');
        //echo $sql = $this->db->last_query();die;
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }


   /******************************************************************************************
    Purpose: Delete pacakges from the cart of current user
    Parameters: packgeId
    ********************************************************************************************/

    function emptyUserCart($userId) {
        $this->db->set('status', 0);
        $this->db->where('userId', $userId);
        $query = $this->db->update('tbl_cart');
        //echo $sql = $this->db->last_query();die;
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }



 /******************************************************************************************
     Purpose: To insert Test info in database added by a user
    Parameters: Package Id, User Id
    ********************************************************************************************/
    function addPackageInCart($packageId, $userId) {
        $this->db->select('*');
        $this->db->from('tbl_cart');
        $this->db->where('userId', $userId);
        $this->db->where('packageId', $packageId);
        $this->db->where('status', 1);
        $query = $this->db->get();
        $num = $query->num_rows();
        if ($num > 0) {
           if (!empty($packageId) || !empty($userId)) {
           $data = $this->getCartList($userId);          
           $previousCount = $data[0]->testCount; 
          // echo $previousCount;die;
           $updateArr = array(
           'testCount'=>$previousCount + 1
           );
           $this->db->where('userId',$userId);
           $this->db->where('packageId',$packageId);
           $this->db->where('status',1);
           $query = $this->db->update('tbl_cart',$updateArr);        
           }         
        } else {
            if (!empty($packageId) || !empty($userId)) {
              /*  $arr = array('userId' => $userId, 'packageId' => $packageId);
                $query = $this->db->query('insert into tbl_cart (userId,packageId) values ("' . $userId . '","' . $packageId . '")');*/
               
               $dataArr = array(
                   'userId'=>$userId,
                   'packageId'=>$packageId,
                   'testCount'=>1
                   );                  
                   $query = $this->db->insert('tbl_cart',$dataArr);
                        
            }
        }
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }

  }  
   
?>
