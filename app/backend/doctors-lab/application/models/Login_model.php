<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Login_model extends CI_Model {
    public function __construct() {
        // Call the Model constructor
        parent::__construct();
    }


    /******************************************************************************************
    @Author: Suika
    Function: For Login Check
    Parameters: Username & password
    ********************************************************************************************/
    function checklogin($userName, $password) {
        $this->load->library('bcrypt');
        $userName = $this->security->xss_clean($userName);
        $password = $this->security->xss_clean($password);
        $this->db->select('userId,firstName,lastName,userName,password');
        $this->db->from('tbl_users');
        $this->db->where('userName', $userName);
        $query = $this->db->get();  
      // echo $sql = $this->db->last_query();die;     
        $data = $query->row();
        //echo "<pre>"; print_r($data);die;
        $userId = $data->userId;
        $PWD = $data->password;
        //echo $this->bcrypt->check_password($password, $PWD);die;
        if ($userId != '') {
            if ($this->bcrypt->check_password($password, $PWD)) {              
                return $data;
            } else {
                return 0;
            }
        }
    }
  }  
   
?>
