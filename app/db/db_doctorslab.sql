-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 30, 2020 at 03:16 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_doctorslab`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cart`
--

CREATE TABLE `tbl_cart` (
  `id` int(11) NOT NULL,
  `userId` int(5) NOT NULL,
  `packageId` varchar(25) DEFAULT NULL,
  `testCount` int(11) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT 1 COMMENT '1- Active 0-Deleted',
  `addedDate` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_cart`
--

INSERT INTO `tbl_cart` (`id`, `userId`, `packageId`, `testCount`, `status`, `addedDate`) VALUES
(1, 1, '1', 6, 0, '2020-08-30 06:26:24'),
(2, 1, '2', 3, 0, '2020-08-30 06:27:09'),
(3, 1, '1', 1, 1, '2020-08-30 06:42:22');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_packages`
--

CREATE TABLE `tbl_packages` (
  `id` int(11) NOT NULL,
  `itemId` text DEFAULT NULL,
  `fasting` int(11) DEFAULT NULL,
  `objectID` int(11) DEFAULT NULL,
  `Keyword` text DEFAULT NULL,
  `url` text DEFAULT NULL,
  `itemName` text DEFAULT NULL,
  `S.no` int(11) DEFAULT NULL,
  `labName` text DEFAULT NULL,
  `testCount` int(11) DEFAULT NULL,
  `availableAt` int(11) DEFAULT NULL,
  `_highlightResult` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`_highlightResult`)),
  `popular` text DEFAULT NULL,
  `Included Tests` text DEFAULT NULL,
  `minPrice` int(11) DEFAULT NULL,
  `type` text DEFAULT NULL,
  `category` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_packages`
--

INSERT INTO `tbl_packages` (`id`, `itemId`, `fasting`, `objectID`, `Keyword`, `url`, `itemName`, `S.no`, `labName`, `testCount`, `availableAt`, `_highlightResult`, `popular`, `Included Tests`, `minPrice`, `type`, `category`) VALUES
(1, 'DIANM11', 0, 6045500, 'covid-19-test', 'covid-19-test', 'COVID-19 Test', 19956, 'Metropolis', 4, 1, '{\"Keyword\": {\"value\": \"covid-19-test\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"path\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"COVID-19 Test\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'TRUE', '', 4500, 'Test', 'path'),
(2, 'DIA2044', 0, 4562, 'eye,test', 'eye_test', 'Eye Test- Vision Express', 1995, 'Vision Express', 1, 1, '{\"Keyword\": {\"value\": \"eye,test\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"path\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"Eye Test- Vision Express\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'TRUE', '', 49, 'Test', 'path'),
(3, 'DIAR894', 0, 4461302, 'Yttrium,Therapy', 'Yttrium-Therapy-test-cost', 'Yttrium Therapy', 1983, '', 1, 2, '{\"Keyword\": {\"value\": \"Yttrium,Therapy\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"Yttrium Therapy\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 17500, 'Test', 'radio'),
(4, 'DIAR893', 0, 4461292, 'X,Ray,Wrist,Lateral,View', 'X-Ray-Wrist-Lateral-View-test-cost', 'X Ray Wrist Lateral View', 1982, '', 1, 2, '{\"Keyword\": {\"value\": \"X,Ray,Wrist,Lateral,View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"X Ray Wrist Lateral View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 120, 'Test', 'radio'),
(5, 'DIAR892', 0, 4461282, 'X,Ray,Wrist,AP,View', 'X-Ray-Wrist-AP-View-test-cost', 'X Ray Wrist AP View', 1981, '', 1, 2, '{\"Keyword\": {\"value\": \"X,Ray,Wrist,AP,View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"X Ray Wrist AP View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 120, 'Test', 'radio'),
(6, 'DIAR891', 0, 4461272, 'X,Ray,Wrist,AP,and', 'X-Ray-Wrist-AP-and-Lateral-View-test-cost', 'X Ray Wrist AP and Lateral View', 1980, '', 1, 2, '{\"Keyword\": {\"value\": \"X,Ray,Wrist,AP,and\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"X Ray Wrist AP and Lateral View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 240, 'Test', 'radio'),
(7, 'DIAR890', 0, 4461262, 'X,Ray,Whole,Spine,Lateral', 'X-Ray-Whole-Spine-Lateral-View-test-cost', 'X Ray Whole Spine Lateral View', 1979, '', 1, 2, '{\"Keyword\": {\"value\": \"X,Ray,Whole,Spine,Lateral\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"X Ray Whole Spine Lateral View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 320, 'Test', 'radio'),
(8, 'DIAR889', 0, 4461252, 'X,Ray,Whole,Spine,Lateral', 'X-Ray-Whole-Spine-Lateral-and-AP-View-test-cost', 'X Ray Whole Spine Lateral and AP View', 1978, '', 1, 2, '{\"Keyword\": {\"value\": \"X,Ray,Whole,Spine,Lateral\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"X Ray Whole Spine Lateral and AP View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 560, 'Test', 'radio'),
(9, 'DIAR888', 0, 4461242, 'X,Ray,Whole,Spine,AP', 'X-Ray-Whole-Spine-AP-View-test-cost', 'X Ray Whole Spine AP View', 1977, '', 1, 2, '{\"Keyword\": {\"value\": \"X,Ray,Whole,Spine,AP\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"X Ray Whole Spine AP View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 320, 'Test', 'radio'),
(10, 'DIAR887', 0, 4461232, 'X,Ray,Water,View,', 'X-Ray-Water-View-test-cost', 'X Ray Water View', 1976, '', 1, 2, '{\"Keyword\": {\"value\": \"X,Ray,Water,View,\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"X Ray Water View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 145, 'Test', 'radio'),
(11, 'DIAR886', 0, 4461222, 'X,Ray,Tm,Joint,Lateral', 'X-Ray-Tm-Joint-Lateral-View-test-cost', 'X Ray Tm Joint Lateral View', 1975, '', 1, 2, '{\"Keyword\": {\"value\": \"X,Ray,Tm,Joint,Lateral\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"X Ray Tm Joint Lateral View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 162, 'Test', 'radio'),
(12, 'DIAR885', 0, 4461212, 'X,Ray,Tm,Joint,AP', 'X-Ray-Tm-Joint-AP-View-test-cost', 'X Ray Tm Joint AP View', 1974, '', 1, 2, '{\"Keyword\": {\"value\": \"X,Ray,Tm,Joint,AP\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"X Ray Tm Joint AP View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 162, 'Test', 'radio'),
(13, 'DIAR884', 0, 4461202, 'X,Ray,Tm,Joint,AP', 'X-Ray-Tm-Joint-AP-and-Lateral-View-test-cost', 'X Ray Tm Joint AP and Lateral View', 1973, '', 1, 2, '{\"Keyword\": {\"value\": \"X,Ray,Tm,Joint,AP\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"X Ray Tm Joint AP and Lateral View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 280, 'Test', 'radio'),
(14, 'DIAR883', 0, 4461192, 'X,Ray,Thumb,Lateral,View', 'X-Ray-Thumb-Lateral-View-test-cost', 'X Ray Thumb Lateral View', 1972, '', 1, 2, '{\"Keyword\": {\"value\": \"X,Ray,Thumb,Lateral,View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"X Ray Thumb Lateral View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 120, 'Test', 'radio'),
(15, 'DIAR882', 0, 4461182, 'X,Ray,Thumb,Lateral,and', 'X-Ray-Thumb-Lateral-and-AP-View-test-cost', 'X Ray Thumb Lateral and AP View', 1971, '', 1, 2, '{\"Keyword\": {\"value\": \"X,Ray,Thumb,Lateral,and\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"X Ray Thumb Lateral and AP View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 240, 'Test', 'radio'),
(16, 'DIAR881', 0, 4461172, 'X,Ray,Thumb,AP,View', 'X-Ray-Thumb-AP-View-test-cost', 'X Ray Thumb AP View', 1970, '', 1, 2, '{\"Keyword\": {\"value\": \"X,Ray,Thumb,AP,View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"X Ray Thumb AP View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 120, 'Test', 'radio'),
(17, 'DIAR880', 0, 4461162, 'X,Ray,Thigh,Lateral,View', 'X-Ray-Thigh-Lateral-View-test-cost', 'X Ray Thigh Lateral View', 1969, '', 1, 2, '{\"Keyword\": {\"value\": \"X,Ray,Thigh,Lateral,View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"X Ray Thigh Lateral View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 120, 'Test', 'radio'),
(18, 'DIAR879', 0, 4461152, 'X,Ray,Thigh,AP,View', 'X-Ray-Thigh-AP-View-test-cost', 'X Ray Thigh AP View', 1968, '', 1, 2, '{\"Keyword\": {\"value\": \"X,Ray,Thigh,AP,View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"X Ray Thigh AP View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 120, 'Test', 'radio'),
(19, 'DIAR878', 0, 4461142, 'X,Ray,Thigh,AP,and', 'X-Ray-Thigh-AP-and-Lateral-View-test-cost', 'X Ray Thigh AP and Lateral View', 1967, '', 1, 2, '{\"Keyword\": {\"value\": \"X,Ray,Thigh,AP,and\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"X Ray Thigh AP and Lateral View\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 240, 'Test', 'radio'),
(20, 'DIAR877', 0, 4461132, 'X,ray,Temp', 'X-ray-Temp-test-cost', 'X ray Temp', 1966, '', 1, 2, '{\"Keyword\": {\"value\": \"X,ray,Temp\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"X ray Temp\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 0, 'Test', 'radio'),
(21, 'DIAR877', 0, 4461132, 'X,ray,Temp', 'X-ray-Temp-test-cost', 'X ray Temp', 1998, '', 1, 2, '{\"Keyword\": {\"value\": \"X,ray,Temp\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"X ray Temp\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 0, 'Test', 'radio'),
(22, 'DIAR877', 0, 4461132, 'X,ray,Temp', 'X-ray-Temp-test-cost', 'X ray Temp', 4455, '', 1, 2, '{\"Keyword\": {\"value\": \"X,ray,Temp\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"X ray Temp\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 0, 'Test', 'radio'),
(23, 'DIAR877', 0, 4461132, 'X,ray,Temp', 'X-ray-Temp-test-cost', 'X ray Temp', 8787, '', 1, 2, '{\"Keyword\": {\"value\": \"X,ray,Temp\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"X ray Temp\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 0, 'Test', 'radio'),
(24, 'DIAR877', 0, 4461132, 'X,ray,Temp', 'X-ray-Temp-test-cost', 'X ray Temp', 12349, '', 1, 2, '{\"Keyword\": {\"value\": \"X,ray,Temp\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"category\": {\"value\": \"radio\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"itemName\": {\"value\": \"X ray Temp\", \"matchLevel\": \"none\", \"matchedWords\": []}, \"Included Tests\": {\"value\": \"\", \"matchLevel\": \"none\", \"matchedWords\": []}}', 'false', '', 0, 'Test', 'radio');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `userId` int(11) NOT NULL,
  `firstName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) DEFAULT NULL,
  `userName` varchar(45) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `added_date` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`userId`, `firstName`, `lastName`, `userName`, `password`, `status`, `added_date`) VALUES
(1, 'Admin', NULL, 'admin', '$2y$12$iUTF6UjfpbtWdbki.ieWBu8ZRRbBG0lhHQ3kCXVr9UWXLrkiS3lQ2', NULL, '2020-08-28 17:19:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_packages`
--
ALTER TABLE `tbl_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_packages`
--
ALTER TABLE `tbl_packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
