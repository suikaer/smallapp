import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/components/services/cart/cart.service';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/components/services/auth/authentication.service';
import { User } from 'src/app/components/models/user/user';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  private cartProductCount: number = 0;
  userId:number;
  constructor(private cartService:CartService,private router: Router,
    private authenticationService: AuthenticationService) {
      const info=JSON.parse(localStorage.getItem('currentUser'));
      this.userId=info['user'].id;
     }

  ngOnInit() {
    this.cartService.event.subscribe(res => {
      this.cartCount();
  })
  this.cartCount();
  }

  currentUser: User;
  
  logout() {
      this.authenticationService.logout();
      this.router.navigate(['/login']);
  }

  cartCount()
  {
    this.cartService.cartProductCount(this.userId).subscribe(
      response=>{ 
          this.cartProductCount=response['count'];
      },err=>{
        console.error(err);
      }
    )
  }
  


}
