import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/components/services/auth/authentication.service';
import { AlertService } from 'src/app/components/services/auth/alert.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  /*******Variables Declaration *****/
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(private formBuilder: FormBuilder,private route: ActivatedRoute,
    private authenticationService:AuthenticationService,private router: Router) {
      authenticationService.logout();
     }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
  });
  this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  
 /*****************************************************************
          Puropse: To save data of form
          Paramters: Username & password
 *********************************************************************/
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    }

    this.loading = true;
    this.authenticationService.login(this.f.username.value,this.f.password.value)
        .pipe(first())
        .subscribe(
          data => {
            if(data.status!=500 && data.status!=0){
            this.router.navigate(["lab-tests"]);
            this.error = data.message;
            this.loading = false;
            }else{
            this.router.navigate(['/login']);
             this.error = data.message;
             this.loading = false;
            }
            console.log("data" + data.status);
        },
        error => {
          
            this.error = error;
            this.loading = false;
        });
}
  

}

