import { Component, OnInit } from '@angular/core';
import { TestlistService } from 'src/app/components/services/testlist/testlist.service';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/components/services/auth/authentication.service';
import { Testlab } from 'src/app/components/models/testlab/testlab';
import { User } from 'src/app/components/models/user/user';
import { CartService } from 'src/app/components/services/cart/cart.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-testlist',
  templateUrl: './testlist.component.html',
  styleUrls: ['./testlist.component.css']
})
export class TestlistComponent implements OnInit {
/*****************Variables Decalartion************* */
public productCount: number ;
currentUser: User;
userid:number;
public testListRecord=[];
public searchdisplay:boolean=false;
public filterPackageListData:Testlab[];
public msg:string='';
public isVisible: boolean = false;
searchText:string;
  constructor(private formBuilder: FormBuilder,private testlistService:TestlistService, private router: Router,
    private authenticationService: AuthenticationService, private cartService:CartService) 
      {
  
        // to get current user Info from local storage
        this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
        const info=JSON.parse(localStorage.getItem('currentUser'));
        this.userid=info['user'].id;
       }
  

  ngOnInit() {
    this.getAlltestList();
  
  }
  
  /************************************************************************
          Puropse: To get all Test / Packages stored in database
        
 **************************************************************************/

private getAlltestList() { 
  this.testlistService.labListdata().subscribe(
   (labtest:Testlab[])=>{     
      this.testListRecord = labtest['list'];
    },err=>{
      console.error(err);
    }
    
  )
}



 /************************************************************************
          Puropse: To add Package/Test in cart and also save  in database
          Parameters: Product Id, User Id

 **************************************************************************/
addCart(labItem)
{ 
  this.cartService.addcart(labItem.id,this.userid).subscribe(
    response=>{ 
          this.productCount=response['cartcount'];
          this.cartService.emitEvent(this.productCount);
          this.msg=response['message'];
          if (this.isVisible) { 
            return;
          } 
          this.isVisible = true;
          setTimeout(()=> this.isVisible = false,2500)
    },err=>{
      console.error(err);
    }
  )
}

 
}
