import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/components/models/user/user';
import { Cart } from 'src/app/components/models/cart/cart';
import { AuthenticationService } from 'src/app/components/services/auth/authentication.service';
import { Router } from '@angular/router';
import { CartService } from 'src/app/components/services/cart/cart.service';
import { TestlistService } from 'src/app/components/services/testlist/testlist.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
/*******************Variables Decaraltion*************/
currentUser: User;
userId:number;
packageCount:number;
cartTotal: number = 0;
packages:Cart[];
Showcheckout:boolean=false;
cartinfohide:boolean=true;
  constructor(private authenticationService: AuthenticationService,private router: Router,
    private cartService:CartService,private testlistService:TestlistService) {
      //get the current user info 
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    const info=JSON.parse(localStorage.getItem('currentUser'));
    this.userId=info['user'].id; 
  }

  ngOnInit() {
    console.log(this.packageCount);
    this.usercartList();
  }

  /************************************************************************
          Puropse: To Show the cart items list added of particular user in database
          Parameters: User Id

 **************************************************************************/
usercartList()
{
  
  this.cartService.cartListByUser(this.userId).subscribe(
   (cart:Cart[])=>{      
  
      this.packages = cart['list'];
      this.updateCartTotal();
    },err=>{
      console.error(err);
    }
    
  )
}

/************************************************************************
          Puropse: To update Total Value on changes of Qty of particlar Package/Test

 **************************************************************************/
updateCartTotal() {
  let total = 0;
  this.packages.map( elem => total = total + elem.testCount*elem.minPrice);
  this.cartTotal = total;
  }

  
  /************************************************************************
          Puropse: To Delete the Health Test form cart
          Parameters: ProductId

 **************************************************************************/
 
 del(packageId)
 {
 
  this.cartService.delUsercart(packageId,this.userId).subscribe(
    (cart:Cart[])=>{      
      this.packageCount=cart['cartcount'];
      this.usercartList();
      this.cartService.emitEvent(this.packageCount);
     },err=>{
       console.error(err);
     }
     
   )
 }

 /************************************************************************
          Puropse: To Increase the qty of package added in cart
          Parameters: Package/Test all Info

 **************************************************************************/
increase(packages)
{
  if(typeof packages.testCount === 'undefined'  ) {
    packages.testCount = 0;
 }
 packages.testCount++;
 this.updateCartTotal();
}

/************************************************************************
          Puropse: To decrease the qty of package added in cart
          Parameters: Package/Test all Info

 **************************************************************************/
decrease(packages)
{
  console.log("bjh  "+packages.testCount);
  if(typeof packages.testCount === 'undefined') {
    packages.testCount = 0;
 }
 if(packages.testCount>0){
 packages.testCount--;
 this.updateCartTotal()
 }
;
}

}
