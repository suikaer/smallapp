import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/components/services/auth/authentication.service';
import { Router } from '@angular/router';
import { CartService } from 'src/app/components/services/cart/cart.service';
import { User } from 'src/app/components/models/user/user';
import { Cart } from 'src/app/components/models/cart/cart';

@Component({
  selector: 'app-place-order',
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.css']
})
export class PlaceOrderComponent implements OnInit {
/*******************Variables Decaraltion*************/
currentUser: User;
userId:number;
packageCount:number;
cartTotal: number = 0;
packages:Cart[];
  constructor(private authenticationService: AuthenticationService,private router: Router,
    private cartService:CartService) {
      //get the current user info 
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    const info=JSON.parse(localStorage.getItem('currentUser'));
    this.userId=info['user'].id; 
  }

  ngOnInit() {
    this.emptyCart();
  }



    /************************************************************************
          Puropse: To Delete the Health Test form cart
          Parameters: ProductId

 **************************************************************************/
 
 emptyCart()
 {
 
  this.cartService.emptyUserCart(this.userId).subscribe(
    (cart:Cart[])=>{      
      this.packageCount=cart['cartcount'];      
      this.cartService.emitEvent(this.packageCount);
     },err=>{
       console.error(err);
     }
     
   )
 }
}
