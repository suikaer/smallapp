import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Testlab } from '../../models/testlab/testlab';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { retry, catchError } from 'rxjs/operators';
import { handleError } from 'src/app/helpers/exception-handling';
import { Cart } from '../../models/cart/cart';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private http: HttpClient) {
  }



/****************************************************************************
          Puropse: Save selected tests/packages to the database
          Paramters: Product Id, Usrr Id
 *******************************************************************************/
addcart(packageId:number,userId:number) : Observable<Testlab[]>{
        
  return this.http.post<Testlab[]>(`${environment.apiUrl}packages/addcart`,{ packageId,userId }).pipe(
   retry(1),
   catchError(handleError)
 );
  }

/****************************************************************************
    Puropse: To get cart count of selected tests/packages of particular user 
    from the database
    Paramters: userId
*******************************************************************************/
  cartProductCount(userId:number) : Observable<Testlab[]>{
    return this.http.post<Testlab[]>(`${environment.apiUrl}packages/cartcount`,{ userId }).pipe(
     retry(1),
     catchError(handleError)
   );
    }

/****************************************************************************
    Puropse: To observe the changes occur while adding the pacakge into 
    cart and changes the cart count accordingly
  
*******************************************************************************/
    private fireEvent = new Subject<number>();
    event = this.fireEvent.asObservable();
    emitEvent(pacakgeCount: number) {
    this.fireEvent.next(pacakgeCount);
    }


     /*****************************************************************
          Puropse: Fetch All list added in Cart of particular User from Database
          Paramters:  User ID
 *********************************************************************/

  cartListByUser(userId:number) : Observable<Cart[]>{
    return this.http.post<Cart[]>(`${environment.apiUrl}/packages/getCartList`,{userId}).pipe(
     retry(1),
     catchError(handleError)
   );
    }

/*****************************************************************
          Puropse: To Delete data of particular User from Database
          Paramters:Package Id & User ID
 *********************************************************************/
    
    delUsercart(packgeId:number,userId:number) : Observable<Cart[]>{
      return this.http.post<Cart[]>(`${environment.apiUrl}/packages/delUsercart`,{packgeId,userId}).pipe(
       retry(1),
       catchError(handleError)
     );
      }


      /*****************************************************************
          Puropse: To empty cart data of particular User from Database
          Paramters:  User ID
          *********************************************************************/
    
   emptyUserCart(userId:number) : Observable<Cart[]>{
      return this.http.post<Cart[]>(`${environment.apiUrl}/packages/emptyUserCart`,{userId}).pipe(
       retry(1),
       catchError(handleError)
     );
      }

}
