import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/layout/common/header/header.component';
import { FooterComponent } from './components/layout/common/footer/footer.component';
import { LeftmenuComponent } from './components/layout/common/leftmenu/leftmenu.component';
import { TestlistComponent } from './components/layout/site/testlist/testlist.component';
import { CartComponent } from './components/layout/site/cart/cart.component';
import { LoginComponent } from './components/layout/site/login/login.component';
import { AuthInterceptorsService } from './http-interceptors/auth-interceptors.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ErrorInterceptorService } from './http-interceptors/error-interceptors.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NotFoundComponent } from './components/layout/site/not-found/not-found.component';
import { CheckoutComponent } from './components/layout/site/checkout/checkout/checkout.component';
import { FilterpipePipe } from './components/pipe/filterpipe.pipe';
import { PlaceOrderComponent } from './components/layout/site/placeOrder/place-order/place-order.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LeftmenuComponent,
    TestlistComponent,
    CartComponent,
    LoginComponent,
    NotFoundComponent,
    CheckoutComponent,
    FilterpipePipe,
    PlaceOrderComponent
  ],
  imports: [
    BrowserModule, 
    FormsModule,     
    ReactiveFormsModule,
    HttpClientModule, 
    AppRoutingModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorsService, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
