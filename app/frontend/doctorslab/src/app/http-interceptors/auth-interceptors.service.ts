import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AuthenticationService } from '../components/services/auth/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorsService implements HttpInterceptor {


  constructor(private authenticationService: AuthenticationService,private router: Router) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      // add authorization header with jwt token if available
      let currentUser = this.authenticationService.currentUserValue;
      if (currentUser && currentUser.token) {
          request = request.clone({
              setHeaders: { 
                  Authorization: `${currentUser.token}`,
                  //Authorization :"Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzYXVyYWJoIiwiZXhwIjoxNTk0ODI1MzQ3LCJpYXQiOjE1OTQ3ODkzNDd9.7DkTRO72x4Y-DXQU_a2GemTFcsUDMF0i5HnUFWjHDgc",
                //  "Content-Type": "multipart/form-data"
              }
          });
      }else
      {
        this.authenticationService.logout();
        this.router.navigate(['/login']);
      }

      return next.handle(request);
  }
}
