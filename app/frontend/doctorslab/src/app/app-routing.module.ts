import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/layout/site/login/login.component';
import { TestlistComponent } from './components/layout/site/testlist/testlist.component';
import { CartComponent } from './components/layout/site/cart/cart.component';
import { AuthGuard } from './helpers/authguard';
import { NotFoundComponent } from './components/layout/site/not-found/not-found.component';
import { CheckoutComponent } from './components/layout/site/checkout/checkout/checkout.component';
import { PlaceOrderComponent } from './components/layout/site/placeOrder/place-order/place-order.component';


const routes: Routes = [
  {path:'login',component:LoginComponent},
  {path:'lab-tests',component:TestlistComponent,canActivate: [AuthGuard]},
  {path:'cart',component:CartComponent,canActivate: [AuthGuard]},
  {path:'checkout',component:CheckoutComponent,canActivate: [AuthGuard]},
  {path:'place-order',component:PlaceOrderComponent,canActivate: [AuthGuard]},
  {path:'pageNotFound',component:NotFoundComponent,canActivate: [AuthGuard]},
  {path:'**',redirectTo:'pageNotFound'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
